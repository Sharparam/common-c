#ifndef INPUT_H
#define INPUT_H

char readChar();
void readString(char *str, int size);
int readInt();
int safeReadInt(char *errorMsg);
long safeReadLong(char *errorMsg);
int readIntRange(int min, int max);
float readFloat();
double readDouble();

#endif
