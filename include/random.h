#ifndef RANDOM_H
#define RANDOM_H

/**
 * Generates a random number as defined by rand().
 * @return A random number in the range [0,RAND_MAX).
 */
int randomInt();

/**
 * Generates a random number with a max bound.
 * @param max The exclusive upper bound.
 * @return A random number in the range [0,max).
 */
int randomMax(int max);

/**
 * Generates a random number within the specified range.
 * @param min The inclusive lower bound.
 * @param max The exclusive upper bound.
 * @return A random number in the range [min,max).
 */
int randomRange(int min, int max);

#endif
