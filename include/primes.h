#ifndef PRIMES_H
#define PRIMES_H

#include <stdbool.h>

/**
 * \brief Determines whether a number is a prime.
 * \param number The number to test.
 * \return true if the number is a prime number, otherwise false.
 */
bool is_prime(int number);

/**
 * \brief Frees the prime memory storage.
 */
void free_prime_memory();

#endif
