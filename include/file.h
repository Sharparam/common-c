#ifndef FILE_H
#define FILE_H

#include <stdbool.h>

bool fileExists(const char *path);
bool fileCanRead(const char *path);
bool fileCanWrite(const char *path);
bool fileCanAccess(const char *path, int flags);

#endif
