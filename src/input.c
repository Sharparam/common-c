#include "input.h"
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

const int MAX_LENGTH_SZ = 256;

char readChar()
{
    char result;
    char input[MAX_LENGTH_SZ];
    readString(input, MAX_LENGTH_SZ);
    sscanf(input, "%c", &result);
    return result;
}

void readString(char *str, int size)
{
    assert(size > 0);
    fgets(str, size, stdin);
    size_t len = strlen(str);
    str[len - 1] = '\0';
    if (len >= 2 && str[len - 2] == '\n')
        str[len - 2] = '\0';
}

int readInt()
{
    int result;
    char input[MAX_LENGTH_SZ];
    readString(input, MAX_LENGTH_SZ);
    sscanf(input, "%d", &result);
    return result;
}

int safeReadInt(char *errorMsg)
{
    long ret = safeReadLong(errorMsg);
    while (ret > INT_MAX || ret < INT_MIN)
    {
        printf("%s", errorMsg);
        ret = safeReadLong(errorMsg);
    }
    return (int)ret;
}

long safeReadLong(char *errorMsg)
{
    long result;
    char *garbage = NULL;
    do
    {
        if (garbage != NULL && *garbage)
            printf("%s", errorMsg);
        char input[MAX_LENGTH_SZ];
        readString(input, MAX_LENGTH_SZ);
        result = strtol(input, &garbage, 10);
    } while (*garbage);

    return result;
}

int readIntRange(int min, int max)
{
    printf("Input a number: [%d-%d] ", min, max);
    int val = readInt();
    while (val < min || val > max)
    {
        printf("Invalid value, try again: [%d-%d] ", min, max);
        val = readInt();
    }
    return val;
}

float readFloat()
{
    float result;
    char input[MAX_LENGTH_SZ];
    readString(input, MAX_LENGTH_SZ);
    sscanf(input, "%f", &result);
    return result;
}

double readDouble()
{
    double result;
    char input[MAX_LENGTH_SZ];
    readString(input, MAX_LENGTH_SZ);
    sscanf(input, "%lf", &result);
    return result;
}
