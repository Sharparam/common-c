#include "file.h"

#ifdef _WIN32

#include <io.h>
#define ACCESS _access
#define F_OK 0
#define W_OK 2
#define R_OK 4

#else

#include <unistd.h>
#define ACCESS access

#endif

bool fileExists(const char *path)
{
    return fileCanAccess(path, F_OK);
}

bool fileCanRead(const char *path)
{
    return fileCanAccess(path, R_OK);
}

bool fileCanWrite(const char *path)
{
    return fileCanAccess(path, W_OK);
}

bool fileCanAccess(const char *path, int flags)
{
    return ACCESS(path, flags) != -1;
}
