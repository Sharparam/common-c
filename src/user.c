#include "user.h"

#ifdef _WIN32
#include <string.h>
#else
#include <unistd.h>
#endif

void getUsername(char *out, size_t outSize)
{
    #ifdef _WIN32
    strcpy(out, "Anonymous");
    #else
    getlogin_r(out, outSize);
    #endif
}
