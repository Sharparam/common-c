#include "helpers.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32

#include <conio.h>
#define WAIT() printf("Press any key to continue...");\
               getch();

#else

#define WAIT() printf("Press ENTER to continue...");\
               getchar();

#endif

void waitForKey()
{
    WAIT()
}
