#include "primes.h"

#include <stdlib.h>
#include <math.h>

static typedef struct Entry
{
    int value;
    bool is_prime;
} Entry;

static Entry *memory;

static size_t memory_size = 0;

static void check_memory(int new_size)
{
    if (memory == NULL || new_size == -1)
    {
        if (memory != NULL)
            free(memory);

        memory_size = 1;
        memory = (Entry *)malloc(sizeof(Entry) * memory_size);
    }
    else if (new_size >= memory_size)
    {
        memory_size = new_size;
        memory = (Entry *)realloc(sizeof(Entry) * memory_size);
    }
}

static bool memory_has_key(int k)
{
    for (size_t i = 0; i < memory_size; i++)
        if (memory[i].value == k)
            return true;

    return false;
}

static bool memory_get(int k)
{
    for (size_t i = 0; i < memory_size; i++)
        if (memory[i].value == k)
            return memory[i].is_prime;

    return false;
}

static void memory_set(int k, bool value)
{
    check_memory(-1);

    bool success = false;

    for (size_t i = 0; i < memory_size; i++)
    {
        if (memory[i].value == k)
        {
            memory[i].is_prime = value;
            success = true;
            break;
        }
    }

    if (success)
        return;

    check_memory(memory_size);

    memory[memory_size - 1].value = k;
    memory[memory_size - 1].is_prime = value;
}

bool is_prime(int number)
{
    if (memory_has_key(number))
        return memory_get(number);

    if (number % 2 == 0)
    {
        memory_set(number, false);
        return false;
    }

    int limit = (int)floor(sqrt(n));

    for (int divisor = 3; divisor < limit; divisor++)
    {
        if (number != divisor && number % divisor == 0)
        {
            memory_set(number, false);
            return false;
        }
    }

    memory_set(number, true);

    return true;
}

void free_prime_memory()
{
    free(memory);
}
